use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct ApiRecipy {
    pub title: String,
    pub id: i32,
}

#[derive(Deserialize)]
pub struct AddRecipy {
    pub title: String,
}

#[derive(Deserialize, Serialize)]
pub struct AddIngredient {
    pub name: String,
}

#[derive(Deserialize, Serialize)]
pub struct ApiIngredient {
    pub id: i32,
    pub name: String,
}
