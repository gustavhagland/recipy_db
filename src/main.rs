use std::env;

use dotenv::dotenv;
use recipy_db::app;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    dotenv()?;
    let db_url = env::var("DATABASE_URL")?;
    let connection = sea_orm::Database::connect(&db_url).await?;
    let app = app(connection).await?;

    axum::Server::bind(&"127.0.0.1:8080".parse()?)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}
