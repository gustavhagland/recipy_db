use axum::{
    extract::Extension,
    routing::{get, post},
    Router,
};
use migration::{Migrator, MigratorTrait};
use sea_orm::DatabaseConnection;

pub mod api_objects;

mod handlers;
mod routes;

pub async fn app(connection: DatabaseConnection) -> anyhow::Result<Router> {
    Migrator::up(&connection, None).await?;

    Ok(Router::new()
        .route("/recipes", get(routes::get_recipes))
        .route("/recipy", post(routes::add_recipy))
        .route("/ingredients", get(routes::get_ingredients))
        .route("/ingredient", post(routes::add_ingredient))
        .layer(Extension(connection)))
}
