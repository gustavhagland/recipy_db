use entity::{ingredient, recipy};
use migration::DbErr;
use sea_orm::{ActiveModelTrait, ActiveValue::NotSet, DatabaseConnection, EntityTrait, Set};

pub async fn get_recipes(connection: &DatabaseConnection) -> Result<Vec<recipy::Model>, DbErr> {
    recipy::Entity::find().all(connection).await
}

pub async fn add_recipy(connection: &DatabaseConnection, title: String) -> Result<(), DbErr> {
    recipy::ActiveModel {
        title: Set(title),
        ..Default::default()
    }
    .save(connection)
    .await?;
    Ok(())
}

pub async fn add_ingredient(connection: &DatabaseConnection, name: String) -> anyhow::Result<()> {
    ingredient::ActiveModel {
        name: Set(name),
        id: NotSet,
    }
    .save(connection)
    .await?;
    Ok(())
}

pub async fn get_ingredients(
    connection: &DatabaseConnection,
) -> anyhow::Result<Vec<ingredient::Model>> {
    Ok(ingredient::Entity::find().all(connection).await?)
}

#[cfg(test)]
mod tests {
    use migration::{Migrator, MigratorTrait};

    use super::*;

    async fn connect_and_migrate() -> DatabaseConnection {
        let connection = sea_orm::Database::connect("sqlite::memory:")
            .await
            .expect("Could not connect to db");
        Migrator::up(&connection, None)
            .await
            .expect("Could not run migrations");

        connection
    }

    #[tokio::test]
    async fn add_get() {
        //Given
        let connection = connect_and_migrate().await;
        let recipy_title = "Test recipy".to_string();

        //When
        add_recipy(&connection.clone(), recipy_title.clone())
            .await
            .expect("Could not add recipy");
        let all_recipes = get_recipes(&connection)
            .await
            .expect("Could not get all recipes");

        //Then
        assert_eq!(1, all_recipes.len());
        assert_eq!(recipy_title, all_recipes[0].title);
    }

    #[tokio::test]
    async fn add_get_ingredient() -> anyhow::Result<()> {
        let connection = connect_and_migrate().await;
        let expected_name = "Test ingredient".to_string();

        //When
        add_ingredient(&connection, expected_name.clone()).await?;
        let all_ingredients = get_ingredients(&connection).await?;

        //Then
        assert_eq!(1, all_ingredients.len());
        assert_eq!(expected_name, all_ingredients[0].name);
        Ok(())
    }
}
