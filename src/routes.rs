use axum::{extract::Extension, Json};
use entity::recipy;
use hyper::StatusCode;
use sea_orm::DatabaseConnection;

use crate::{
    api_objects::{AddIngredient, AddRecipy, ApiIngredient, ApiRecipy},
    handlers,
};

impl ApiRecipy {
    pub fn from(db_model: recipy::Model) -> Self {
        Self {
            title: db_model.title,
            id: db_model.id,
        }
    }
}

pub async fn get_recipes(
    Extension(connection): Extension<DatabaseConnection>,
) -> Result<Json<Vec<ApiRecipy>>, StatusCode> {
    let recipes = handlers::get_recipes(&connection)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
        .into_iter()
        .map(|db_model| ApiRecipy::from(db_model))
        .collect();
    Ok(Json(recipes))
}

pub async fn add_recipy(
    Extension(connection): Extension<DatabaseConnection>,
    Json(body): Json<AddRecipy>,
) -> Result<(), StatusCode> {
    handlers::add_recipy(&connection, body.title)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)
}

pub async fn add_ingredient(
    Extension(connection): Extension<DatabaseConnection>,
    Json(ingredient): Json<AddIngredient>,
) -> Result<(), StatusCode> {
    handlers::add_ingredient(&connection, ingredient.name)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)
}

pub async fn get_ingredients(
    Extension(connection): Extension<DatabaseConnection>,
) -> Result<Json<Vec<ApiIngredient>>, StatusCode> {
    Ok(Json(
        handlers::get_ingredients(&connection)
            .await
            .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
            .into_iter()
            .map(|model| ApiIngredient {
                name: model.name,
                id: model.id,
            })
            .collect(),
    ))
}
