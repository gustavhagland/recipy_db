use hyper::{body::to_bytes, Body, Method, Request, StatusCode};
use recipy_db::{
    api_objects::{AddIngredient, ApiIngredient, ApiRecipy},
    app,
};
use sea_orm::Database;
use std::net::{SocketAddr, TcpListener};

async fn start_app() -> anyhow::Result<SocketAddr> {
    let addresses: Vec<SocketAddr> = (0..10)
        .map(|index| SocketAddr::from(([127, 0, 0, 1], 8000 + index)))
        .collect();

    let listener = TcpListener::bind(&*addresses)?;

    let addr = listener.local_addr()?;

    let connection = Database::connect("sqlite::memory:").await?;

    tokio::spawn(async move {
        axum::Server::from_tcp(listener)?
            .serve(
                app(connection)
                    .await
                    .expect("Could not get app")
                    .into_make_service(),
            )
            .await
    });

    Ok(addr)
}

#[tokio::test]
async fn add_get_sunny_day() -> anyhow::Result<()> {
    //Given
    let addr = start_app().await?;
    let client = hyper::Client::new();
    let recipy_title = "test_recipy";

    //When
    let add_response = client
        .request(
            Request::builder()
                .method(Method::POST)
                .uri(format!("http://{}/recipy", addr))
                .header("content-type", "application/json")
                .body(Body::from(format!(r#"{{"title":"{}"}}"#, recipy_title)))?,
        )
        .await?;
    let response = client
        .request(
            Request::builder()
                .uri(format!("http://{}/recipes", addr))
                .body(Body::empty())?,
        )
        .await?;

    //Then
    assert_eq!(StatusCode::OK, response.status());
    assert_eq!(StatusCode::OK, add_response.status());

    let body = response.into_body();
    let recipes: Vec<ApiRecipy> = serde_json::from_slice(&to_bytes(body).await?)?;

    assert_eq!(1, recipes.len());
    assert_eq!(recipy_title, recipes[0].title);

    Ok(())
}

#[tokio::test]
async fn add_get_ingredient() -> anyhow::Result<()> {
    let addr = start_app().await?;
    let client = hyper::Client::new();
    let expected_ingredient = AddIngredient {
        name: "Test ingredient".to_string(),
    };

    //When
    let add_request = client
        .request(
            Request::builder()
                .uri(format!("http://{}/ingredient", addr))
                .method(Method::POST)
                .header("content-type", "application/json")
                .body(Body::from(serde_json::to_string(&expected_ingredient)?))?,
        )
        .await?;

    let ingredients_request = client
        .request(
            Request::builder()
                .uri(format!("http://{}/ingredients", addr))
                .body(Body::empty())?,
        )
        .await?;

    assert_eq!(
        StatusCode::OK,
        add_request.status(),
        "post request was not OK"
    );
    assert_eq!(
        StatusCode::OK,
        ingredients_request.status(),
        "Get request was not OK"
    );

    let ingredients_request_body = ingredients_request.into_body();
    let ingredients: Vec<ApiIngredient> =
        serde_json::from_slice(&to_bytes(ingredients_request_body).await?)?;

    assert_eq!(1, ingredients.len());
    assert_eq!(expected_ingredient.name, ingredients[0].name);

    Ok(())
}
