use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(RecipyIngredient::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(RecipyIngredient::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(RecipyIngredient::IngredientId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(RecipyIngredient::RecipyId)
                            .integer()
                            .not_null(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(RecipyIngredient::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
enum RecipyIngredient {
    Table,
    Id,
    RecipyId,
    IngredientId,
}
