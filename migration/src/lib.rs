pub use sea_orm_migration::prelude::*;

mod m20220814_113638_create_recipes_table;
mod m20220814_141605_create_ingredients_table;
mod m20220814_141617_create_recipy_ingredient_table;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20220814_113638_create_recipes_table::Migration),
            Box::new(m20220814_141605_create_ingredients_table::Migration),
            Box::new(m20220814_141617_create_recipy_ingredient_table::Migration),
        ]
    }
}
